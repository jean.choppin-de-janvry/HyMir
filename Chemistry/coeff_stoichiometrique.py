# On représente une molécule par un dictionnaire.
# exemple : H20 = {'H':2,'O':1}
import numpy as np
from numpy.linalg import solve


def equilibre(reactif, produit, clé):
    """
    reactif : liste de dictionnaire, les dictionnaires representent les molécules entrantes
    produit : liste de dictionnaire, les dictionnaires representent les molécules produites
    clé : dictionnaires des atomes présents
    """
    Representation_matricielle = np.zeros((len(clé), len(reactif) + len(produit)))
    l = []
    k = 0
    for molecule in reactif:
        l.append(molecule)
        for atome in molecule:
            Representation_matricielle[clé[atome], k] = molecule[atome]
        k += 1
    for molecule in produit:
        l.append(molecule)
        for atome in molecule:
            Representation_matricielle[clé[atome], k] = -molecule[atome]
        k += 1

    res = solve(Representation_matricielle[:, 1:], -Representation_matricielle[:, 0])
    dictionnaire_resultat = {str(l[0]): 1}
    for k in range(len(res)):
        dictionnaire_resultat[str(l[k + 1])] = res[k]
    return dictionnaire_resultat


sol = equilibre(
    [{"C": 7, "H": 16}, {"N": 2, "O": 1}],
    [{"N": 2}, {"C": 1, "O": 2}, {"H": 2, "O": 1}],
    {"H": 0, "O": 1, "N": 2, "C": 3},
)

print(sol)
