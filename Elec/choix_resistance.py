# L'objectif de ces fonctions sont d'obtenir des valeurs de resistance réaliste, les calculs ssont fait au sein d'une même décade


def resistance(x, n):
    if x in range(n):  # x entier entre 0 et n-1
        return round(
            10 ** (x / n), 1
        )  # remarque, il y a certains ecarts, à comparer avec le tableau des series normalisees
    else:
        return "valeur d'entrée invalide"


def pont_diviseur(x, y, n):
    return resistance(x, n) / (resistance(y, n) + resistance(x, n))


def opti(val, n):  # val est la valeur que l'on souhaite approcher
    minx, miny, min = 0, 0, abs(pont_diviseur(0, 0, n) - val)
    for k in range(n):
        for l in range(n):
            if abs(pont_diviseur(k, l, n) - val) < min:
                minx, miny, min = k, l, abs(pont_diviseur(k, l, n) - val)
    return resistance(minx, n), resistance(miny, n), pont_diviseur(minx, miny, n)


print(opti(3.3 / 5, 12), 3.3 / 5)
print(opti(3.3 / 10, 12), 3.3 / 10)

# une fois ce résultat théorique trouvé, on peut comparer aux vrais valeurs

vrai_val = [1, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2]
