%CRITICAL ASSUMPTIONS: 
% - OX Mass Flow rate not significantly dependent on chamber pressure 
% - Thus, Ox mass flow is constant 
% - Size nozzle such that flow is fully expanded at sea level 
% - Regression Rate parameter n=0.67 (Allehaut) 



%Getting Started: 
% - Choose Input Parameters in section (A) 
% - Run NASA's CEA program with N2O + Paraffin to determine input 
%   parameters for section (C) 
% Run program to analyze engine performance over time 
% - NOTE: Bugs may be present. Slack me (@Will) if you find anything 
%   wacky 


%Checklist:
% - Relire le code
% - Rajouter le calcul de la température interne
% - Récupérer les valeurs a et n de la paraffine 


clear;
format longg;



% (A) Specify Input Parameters 
initial_thrust = 0; % N 
p_chamber = 4.0e6; % Pa 
grain_inner_radius = 0.0125; % m                                                                           
p_ambient = 101490.83; % Pa 
p_exit = 101490.83; % Pa, assuming perfectly expanded exhaust 
burn_time = 60; % sec 
nozzle_throat_area=pi*((12.8e-3)/2)^2; %mm
nozzle_area_ratio=5.204;
inner_radius = 63.6e-3 /2;




% (B) Hybrid Design Parameters 
n_ports = 1; 



% Propellant Parameters (fuel constants, dependent on oxidizer choice) 
% Currently chosen for N2O + Paraffin (w/ Al) 
propellant_parameter_A = 0.104; %0.104  Note: Values A=0.223, n=0.5 taken from Zilliac et al. for Paraffin+Al fuel,  
propellant_parameter_n = 0.67;% 0.67 Other values taken from COMBUSTION OF PLAIN AND REINFORCED PARAFFIN WITH NITROUS OXIDE, Sisi et al. 
propellant_parameter_m = 0;% For no axial variance 
fuel_density = 900;% kg/m^3           % From Allehaut        


% (C) Attain Additional Parameters from CEA/RPA based off of chosen propellants 
initial_of = 7; % 3.31 
target_isp = 248.3;% sec 
c_star = 1424;% m/sec 
gamma_c = 1.17; %1.135

 



% Constants/Inefficiency constants 
g_c = 9.81;% m/sec^2 

 

% Nozzles: 
C_D_Nozzle = 0.95;% Typical Value from Prince et al. 
discharge_correction = 1.07;% Accounts for molecular weight of exhaust gases, heat transfer to nozzle, gas property changes in nozzle 
energy_correction = 0.928;% Related to energy conversion 
nozzle_efficiency_correction = discharge_correction * energy_correction; 

 

% C_Star: 
c_star_efficiency = 0.875;% Correction for incomplete oxidizer and fuel mixing 

 



%% Begin Calculations 





% Nozzle Characteristics 

 

% Determine area expansion ratio, assuming p_exit = p_ambient 
% Note: Solves for Ideal expansion ratio, may need to pick a more feasible 
% ratio, then implement root finding to determine exit pressure 



% nozzle_area_ratio = 1 / (((gamma_c + 1)/2)^(1/(gamma_c-1)) ... 
%           * (p_ambient/p_chamber)^(1/gamma_c) ... 
%           * sqrt(((gamma_c + 1)/(gamma_c-1)) ... 
%           * (1 - (p_ambient/p_chamber)^((gamma_c-1)/gamma_c)))); 



thrust_coeff = sqrt((2*gamma_c^2)/(gamma_c-1) ... 
         * (2/(gamma_c+1))^((gamma_c+1)/(gamma_c-1)) ... 
         * (1-(p_exit/p_chamber)^((gamma_c-1)/gamma_c))) + nozzle_area_ratio * (p_exit - p_ambient)/p_chamber; 



%nozzle_throat_area = initial_thrust/(nozzle_efficiency_correction * thrust_coeff * p_chamber); 

 

nozzle_exit_area = nozzle_area_ratio * nozzle_throat_area; 

 

 

% Propellant Characteristics 

 

initial_m_dot = (p_chamber * nozzle_throat_area) / (c_star_efficiency * c_star); 



initial_m_dot_f = initial_m_dot/(initial_of + 1); 



initial_m_dot_o = initial_of * initial_m_dot_f; 





% Grain Characteristics 

 

%Option to specify outer radius and calculate e 
%grain_inner_radius = nthroot(( -1 *  burn_time * propellant_parameter_A * (1e-3) ... 
%                    * (2 * propellant_parameter_n + 1) ... 
%                    * (initial_m_dot_o/pi)^propellant_parameter_n  ... 
%                    + (grain_outer_radius)^(2 * propellant_parameter_n + 1)) ... 
%                    , (2 * propellant_parameter_n + 1)); 



initial_ox_flux = initial_m_dot_o/(pi*grain_inner_radius^2); 



initial_regression_rate = (1e-3)*((propellant_parameter_A *(1 - propellant_parameter_n) * (initial_ox_flux^propellant_parameter_n))/(((1 + (1 / initial_of)) ^ (1 - propellant_parameter_n) - 1) * initial_of)); 
% in m/sec, OF corrections based off Karabeyoglu et al: "Scale-Up Tests of 
% High Regression Rate Paraffin-Based Hybrid Rocket Fuels" 

 

grain_length = initial_m_dot_f/(2 * pi * n_ports * grain_inner_radius * fuel_density * initial_regression_rate); 

 

 

% Use First Order Forward Difference Algorithm for coupled time-space 
% problem of propellant burn + thrust generation 
% (Cantwell, AA 283 Course Reader) 

 

 

% Step 1: Specify Port Length, Initial Radius 
% Calculate Dimensionless Variables C_R, C_J 
%(Assuming initial port radius is constant along main axis) 
% Specify dimensionless ox mass flow variable if necessary (lambda) 

 

 

C_R = (1e-3 * propellant_parameter_A * burn_time ... 
    * initial_m_dot_o ^ propellant_parameter_n) ... 
    /(grain_inner_radius^(2 * propellant_parameter_n + propellant_parameter_m + 1)); 



C_J = (1e-3 * propellant_parameter_A * fuel_density ... 
    * initial_m_dot_o^(propellant_parameter_n-1)) ... 
    / (grain_inner_radius^(2 * propellant_parameter_n + propellant_parameter_m - 2)); 

 

i_max = 2000; 
j_max = 2000; 


 

lambda = 0; 

 

% Step 2: Initialize dimensionless coordinates 
% Specify number of steps (spatial --> chi (indexed by i), temporal --> tau (indexed by j)) 

 

i = 1:i_max; 
j = 1:j_max; 



chi(i) = (i/i_max) * (grain_length/grain_inner_radius); % 0 --> L_max/grain_inner_radius 
tau(j) = (j/j_max); % 0 --> 1 





% Step 3: Specify initial port geometry, create table spanning time and 
% space for quanitites R and J 



 

R = zeros(i_max,j_max); 
J = zeros(i_max,j_max); 



for j = 1:j_max 
 
 for i = 1:i_max 
    % Assuming uniform initial grain along main axis 
     R(i, j) = grain_inner_radius/grain_inner_radius;
     J(i, j) = 1; 
 end 
end 


 

 

% Step 4: Iterate on quantities throughout time t: 



delta_chi = (1/i_max) * (grain_length/grain_inner_radius); 
delta_tau = 1/j_max; 



for j = 1:j_max-1 
    for i = 1:i_max-1 
     R(i, j+1) = R(i, j) + delta_tau * (C_R/(chi(i)^propellant_parameter_m)) * (J(i, j)/(pi * R(i, j)^2))^propellant_parameter_n; 
     J(i+1, j) = J(i, j) + delta_chi * (C_J * 2 * pi * R(i, j) /(chi(i)^propellant_parameter_m)) * (J(i, j)/(pi * R(i, j)^2))^propellant_parameter_n; 
     end 
end 



%Calculate final values based off of simulation 



grain_outer_radius = grain_inner_radius * R(1, j_max); 




m_dot_f_integral = cumtrapz((burn_time / j_max) * (1:j_max), J(i_max, 1:j_max) * initial_m_dot_o - initial_m_dot_o); 
 

m_fuel_total = m_dot_f_integral(j_max); 
m_ox_total = initial_m_dot_o * burn_time; 
m_prop_total = m_ox_total + m_fuel_total; 

 

graphs = tiledlayout(3,2); 

list_of_radius = grain_inner_radius * R(1, 1:j_max);

list_of_radius = list_of_radius(list_of_radius<inner_radius);

t_size = length(list_of_radius);


nexttile 
plot((burn_time / j_max) * (1:t_size) , list_of_radius) 
title("Grain Radius vs. Time (Constant Ox Mass Flow)") 
xlabel("Time (sec)") 
ylabel("Grain Radius (m)") 



nexttile 
plot((burn_time / j_max) * (1:t_size), J(i_max, 1:t_size) * initial_m_dot_o - initial_m_dot_o) 
title("Fuel Mass Flow vs. Time (Constant Ox Mass Flow)") 
xlabel("Time (sec)") 
ylabel("Mass Flow rate kg/sec") 
%NOTE: Plot J (dimensionless mass flow rate) at end of grain. 



nexttile 
plot((burn_time / j_max) * (1:t_size), initial_m_dot_o ./ (J(i_max, 1:t_size) * initial_m_dot_o - initial_m_dot_o)) 
title("O/F Ratio vs. Time (Constant Ox Mass Flow)") 
xlabel("Time (sec)") 
ylabel("O/F Ratio") 



nexttile 
plot((burn_time / j_max) * (1:t_size), (initial_m_dot_o + (J(i_max, 1:t_size) * initial_m_dot_o - initial_m_dot_o)) .* c_star .* thrust_coeff .* c_star_efficiency .* 0.97) 
title("Thrust vs. Time (Constant Ox Mass Flow)") 
xlabel("Time (sec)") 
ylabel("Thrust (N)") 



nexttile 
plot((burn_time / j_max) * (1:t_size), (initial_m_dot_o + (J(i_max, 1:t_size) * initial_m_dot_o - initial_m_dot_o)) .* c_star .* c_star_efficiency ./ nozzle_throat_area ./ C_D_Nozzle) 
title("Chamber Pressure vs. Time (Constant Ox Mass Flow)") 
xlabel("Time (sec)") 
ylabel("Chamber Pressure (Pa)")



