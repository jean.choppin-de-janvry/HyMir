import numpy as np
import matplotlib.pyplot as plt

# Specify Input Parameters
p_chamber = 4e6  # Pa
grain_inner_radius = 0.0125  # m
max_grain_radius = (63.6e-3)/2
p_ambient = 101490.83  # Pa
p_exit = 101490.83  # Pa, assuming perfectly expanded exhaust
burn_time = 20  # sec

# Constants/Inefficiency constants
g_c = 9.81  # m/sec^2

# Hybrid Design Parameters
n_ports = 1

# Propellant Parameters
propellant_parameter_A = 0.104 # 0.104
propellant_parameter_n = 0.67 # 0.67
A_values = np.linspace(0.104, 0.3, num=1)
n_values = np.linspace(0.6, 0.69, num=10)
propellant_parameter_m = 0
fuel_density = 900  # kg/m^3

# Attain Additional Parameters
initial_of = 7  # Initial O/F Ratio
target_isp = 2133.1/g_c  # sec
c_star =  1430.5   # m/sec
gamma_c = 1.17 # Specific heat ratio

# Nozzles
C_D_Nozzle = 0.95 
discharge_correction = 1.07
energy_correction = 0.928 
nozzle_efficiency_correction = discharge_correction * energy_correction

# C_Star
c_star_efficiency = 0.875

#problemeforjean = 10/0

# Nozzle Characteristics
nozzle_area_ratio = 5.2041
thrust_coeff = np.sqrt((2 * gamma_c ** 2) / (gamma_c - 1) *
                       (2 / (gamma_c + 1)) ** ((gamma_c + 1) / (gamma_c - 1)) *
                       (1 - (p_exit / p_chamber) ** ((gamma_c - 1) / gamma_c))) + \
               nozzle_area_ratio * (p_exit - p_ambient) / p_chamber
nozzle_throat_area = ((12.8*10**(-3)/2)**2) * np.pi
nozzle_exit_area = nozzle_area_ratio * nozzle_throat_area

# Propellant Characteristics
initial_m_dot = (p_chamber * nozzle_throat_area) / (c_star_efficiency * c_star)
initial_m_dot_f = initial_m_dot / (initial_of + 1)
initial_m_dot_o = initial_of * initial_m_dot_f

def get_arrays(propellant_parameter_A, propellant_parameter_n):
    # Grain Characteristics
    initial_ox_flux = initial_m_dot_o / (np.pi * grain_inner_radius ** 2)
    initial_regression_rate = (1e-3) * ((propellant_parameter_A * (1 - propellant_parameter_n) *
                                        (initial_ox_flux ** propellant_parameter_n)) /
                                        (((1 + (1 / initial_of)) ** (1 - propellant_parameter_n) - 1) * initial_of))
    grain_length = initial_m_dot_f / (2 * np.pi * n_ports * grain_inner_radius * fuel_density * initial_regression_rate)

    # Step 1: Specify Port Length, Initial Radius
    C_R = (1e-3 * propellant_parameter_A * burn_time *
        initial_m_dot_o ** propellant_parameter_n) / (grain_inner_radius ** (2 * propellant_parameter_n + propellant_parameter_m + 1))
    C_J = (1e-3 * propellant_parameter_A * fuel_density *
        initial_m_dot_o ** (propellant_parameter_n - 1)) / (grain_inner_radius ** (2 * propellant_parameter_n + propellant_parameter_m - 2))

    i_max = 500
    j_max = 500

    # Step 2: Initialize dimensionless coordinates
    i = np.arange(1, i_max + 1)
    j = np.arange(1, j_max + 1)

    chi = (i / i_max) * (grain_length / grain_inner_radius)
    tau = j / j_max

    # Step 3: Specify initial port geometry, create table spanning time and space for quanitites R and J
    R = np.zeros((i_max, j_max))
    J = np.zeros((i_max, j_max))

    for jj in range(j_max):
        for ii in range(i_max):
            R[ii, jj] = grain_inner_radius / grain_inner_radius
            J[ii, jj] = 1

    # Step 4: Iterate on quantities throughout time t
    delta_chi = (1 / i_max) * (grain_length / grain_inner_radius)
    delta_tau = 1 / j_max

    for jj in range(j_max - 1):
        for ii in range(i_max - 1):
            R[ii, jj + 1] = R[ii, jj] + delta_tau * (C_R / (chi[ii] ** propellant_parameter_m)) * \
                            (J[ii, jj] / (np.pi * R[ii, jj] ** 2)) ** propellant_parameter_n
            J[ii + 1, jj] = J[ii, jj] + delta_chi * (C_J * 2 * np.pi * R[ii, jj] / (chi[ii] ** propellant_parameter_m)) * \
                            (J[ii, jj] / (np.pi * R[ii, jj] ** 2)) ** propellant_parameter_n

    # Calculate final values based off of simulation
    grain_outer_radius = grain_inner_radius * R[0, j_max - 1]
    m_dot_f_integral = np.trapz((burn_time / j_max) * np.arange(1, j_max + 1),
                                J[i_max - 1, :] * initial_m_dot_o - initial_m_dot_o)
    m_fuel_total = m_dot_f_integral
    m_ox_total = initial_m_dot_o * burn_time
    m_prop_total = m_ox_total + m_fuel_total

    # Plotting
    time = (burn_time / j_max) * np.arange(1, j_max + 1)

    r_copy = np.copy(R[0, :]) * grain_inner_radius
    r_copy = r_copy[r_copy < max_grain_radius]
    max_time = len(r_copy)
    time_cut = time[:max_time]

    return r_copy,J,time_cut,max_time,i_max

plt.figure(figsize=(12, 12))
for i in range(len(A_values)):
    for j in range(len(n_values)):
        r_tot, J_tot, time_cut, max_time, i_max = get_arrays(A_values[i], n_values[j])

        plt.subplot(3, 2, 1)
        plt.plot(time_cut, r_tot, label=f"a={A_values[i]} n={n_values[j]}")
        plt.title("Grain Radius vs. Time (Constant Ox Mass Flow)")
        plt.xlabel("Time (sec)")
        plt.ylabel("Grain Radius (m)")

        plt.subplot(3, 2, 2)
        plt.plot(time_cut, (J_tot[i_max - 1, :] * initial_m_dot_o - initial_m_dot_o)[:max_time], label=f"a={A_values[i]} n={n_values[j]}")
        plt.title("Fuel Mass Flow vs. Time (Constant Ox Mass Flow)")
        plt.xlabel("Time (sec)")
        plt.ylabel("Mass Flow rate kg/sec")

        plt.subplot(3, 2, 3)
        plt.plot(time_cut, (initial_m_dot_o / (J_tot[i_max - 1, :] * initial_m_dot_o - initial_m_dot_o))[:max_time], label=f"a={A_values[i]} n={n_values[j]}")
        plt.title("O/F Ratio vs. Time (Constant Ox Mass Flow)")
        plt.xlabel("Time (sec)")
        plt.ylabel("O/F Ratio")

        plt.subplot(3, 2, 4)
        plt.plot(time_cut, ((initial_m_dot_o + (J_tot[i_max - 1, :] * initial_m_dot_o - initial_m_dot_o)) *
            c_star * thrust_coeff * c_star_efficiency * 0.97)[:max_time], label=f"a={A_values[i]} n={n_values[j]}")
        plt.title("Thrust vs. Time (Constant Ox Mass Flow)")
        plt.xlabel("Time (sec)")
        plt.ylabel("Thrust (N)")

        plt.subplot(3, 2, 5)
        plt.plot(time_cut, ((initial_m_dot_o + (J_tot[i_max - 1, :] * initial_m_dot_o - initial_m_dot_o)) *
            c_star * c_star_efficiency / nozzle_throat_area / C_D_Nozzle)[:max_time], label=f"a={A_values[i]} n={n_values[j]}")
        plt.title("Chamber Pressure vs. Time (Constant Ox Mass Flow)")
        plt.xlabel("Time (sec)")
        plt.ylabel("Chamber Pressure (Pa)")
        plt.legend(bbox_to_anchor=(1.80, 1), loc='upper right', borderaxespad=0.)
        
        end_time=time_cut[max_time-1]
        m_ox_tot=initial_m_dot_o*end_time
        I_tot=np.trapz(((initial_m_dot_o + (J_tot[i_max - 1, :] * initial_m_dot_o - initial_m_dot_o)) *
            c_star * thrust_coeff * c_star_efficiency * 0.97)[:max_time],time_cut)

        print(f"a={round(A_values[i],3)} n={round(n_values[j],3)} end_time={round(end_time,3)} M_ox_totale={round(m_ox_tot,3)} I_tot={I_tot}")

plt.tight_layout()
plt.show()

